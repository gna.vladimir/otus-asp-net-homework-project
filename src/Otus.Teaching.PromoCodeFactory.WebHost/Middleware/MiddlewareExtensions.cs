﻿using Microsoft.AspNetCore.Builder;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Middleware
{
    public static class MiddlewareExtensions
    {
        public static void UseExceptionHandlerMiddleware(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandlerMiddleware>();
        }
    }
}
