﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class DateFilter
    {
        public required string BeginDate { get; set; }

        public required string EndDate { get; set; }
    }
}
