﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeModel
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public RoleModel Role { get; set; }        
    }
}