﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromocodeModel
    {
        public string Code { get; set; }

        public Guid Id { get; set; }

        public string ServiceInfo { get; set; }

        public DateOnly? BeginDate { get; set; }

        public DateOnly? EndDate { get; set; }

        public string PartnerName { get; set; }

        public EmployeeModel PartnerManager { get; set; }

        public PreferenceModel Preference { get; set; }        
    }
}