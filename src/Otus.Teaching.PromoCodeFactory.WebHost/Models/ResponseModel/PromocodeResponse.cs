﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel
{
    public record PromocodeResponse
    (
        Guid Id,
        string Code,
        string ServiceInfo,
        DateOnly BeginDate,
        DateOnly EndDate,
        string PartnerName,
        PersonResponse PartnerManager,
        List<PersonResponse> Customers
    );
}