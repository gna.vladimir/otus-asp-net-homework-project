﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel
{
    public record EmployeeResponse(
        Guid Id,
        string FullName,
        string Email,
        RoleResponse Role,
        int AppliedPromocodesCount);    
}