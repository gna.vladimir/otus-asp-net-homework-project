﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel
{
    public record PreferenceResponse(
        Guid Id,
        string Name
    );
}