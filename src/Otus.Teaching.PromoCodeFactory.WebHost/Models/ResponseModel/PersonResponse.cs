﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel
{
    public record PersonResponse(
        Guid Id,
        string FullName,
        string Email);
}