﻿using System;
using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel
{
    public record CustomerResponse(
        Guid Id,
        string FullName,
        string Email,
        List<PreferenceResponse> Preferences,
        List<PromocodeResponse> Promocodes
    );
}