﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel
{
    public record RoleResponse(
        Guid Id,
        string Name,
        string Description);
}