﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers.AutoMapper
{
    public class PromocodeUiProfile : Profile
    {
        public PromocodeUiProfile()
        {
            CreateMap<PromocodeDto, PromocodeResponse>();

            CreateMap<PromocodeModel, PromocodeDto>()
                .ForMember(x => x.Customers, map => map.Ignore());
        }
    }
}
