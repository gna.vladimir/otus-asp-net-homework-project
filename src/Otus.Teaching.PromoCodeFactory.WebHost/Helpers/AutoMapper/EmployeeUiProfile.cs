﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers.AutoMapper
{
    public class EmployeeUiProfile : Profile
    {
        public EmployeeUiProfile()
        {
            CreateMap<EmployeeDto, EmployeeResponse>();            

            CreateMap<EmployeeDto, PersonResponse>();            

            CreateMap<EmployeeModel, EmployeeDto>()
                .ForMember(x => x.AppliedPromocodesCount, map => map.Ignore());            
        }

    }
}
