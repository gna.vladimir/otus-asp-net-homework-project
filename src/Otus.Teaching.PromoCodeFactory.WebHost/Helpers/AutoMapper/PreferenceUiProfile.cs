﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers.AutoMapper
{
    public class PreferenceUiProfile : Profile
    {
        public PreferenceUiProfile()
        {
            CreateMap<PreferenceDto, PreferenceResponse>();        
            
            CreateMap<PreferenceModel, PreferenceDto>()
                .ForMember(x => x.Customers, map => map.Ignore());
        }
    }
}
