﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel;
using System.Runtime.InteropServices;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers.AutoMapper
{
    public class RoleUiProfile : Profile
    {
        public RoleUiProfile()
        {
            CreateMap<RoleDto, RoleResponse>();
            CreateMap<RoleModel, RoleDto>();
        }
    }
}
