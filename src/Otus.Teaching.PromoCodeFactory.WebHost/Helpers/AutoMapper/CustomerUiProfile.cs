﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Helpers.AutoMapper
{
    public class CustomerUiProfile : Profile
    {
        public CustomerUiProfile()
        {
            CreateMap<CustomerDto, CustomerResponse>();

            CreateMap<CustomerDto, PersonResponse>();

            CreateMap<CustomerModel, CustomerDto>()
                .ForMember(x => x.Promocodes, map => map.Ignore());
        }

    }
}
