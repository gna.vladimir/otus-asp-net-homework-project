﻿using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.WebHost.Settings;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.Service;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Service.Mapping;
using Otus.Teaching.PromoCodeFactory.WebHost.Helpers.AutoMapper;
using Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    internal static class Registrar
    {
        internal static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            var applicationSettings = configuration.Get<ApplicationSettings>();
            services.AddSingleton(applicationSettings);
            return services.AddSingleton((IConfigurationRoot)configuration)
                .InstallServices()
                .ConfigureContext(applicationSettings.ConnectionString)
                .InstallRepositories();
        }

        private static IServiceCollection InstallServices(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()))
                .AddTransient<IRoleService, RoleService>()
                .AddTransient<ICustomerService, CustomerService>()
                .AddTransient<IEmployeeService, EmployeeService>()
                .AddTransient<IPreferenceService, PreferenceService>()
                .AddTransient<IPromocodeService, PromocodeService>();
            
            return serviceCollection;
        }

        private static IServiceCollection InstallRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<IRoleRepository, RoleRepository>()
                .AddTransient<ICustomerRepository, CustomerRepository>()
                .AddTransient<IEmployeeRepository, EmployeeRepository>()
                .AddTransient<IPreferenceRepository, PreferenceRepository>()
                .AddTransient<IPromocodeRepository, PromocodeRepository>();                
            return serviceCollection;
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<RoleProfile>();
                cfg.AddProfile<RoleUiProfile>();
                cfg.AddProfile<EmployeeProfile>();
                cfg.AddProfile<EmployeeUiProfile>();
                cfg.AddProfile<CustomerProfile>();
                cfg.AddProfile<CustomerUiProfile>();
                cfg.AddProfile<PreferenceProfile>();
                cfg.AddProfile<PreferenceUiProfile>();
                cfg.AddProfile<PromocodeProfile>();
                cfg.AddProfile<PromocodeUiProfile>();
            });
            configuration.AssertConfigurationIsValid();
            return configuration;
        }
    }
}
