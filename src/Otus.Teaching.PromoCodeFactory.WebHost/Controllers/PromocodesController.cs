﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Service;
using Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : Controller
    {        
        private readonly IPromocodeService _promocodeService;
        private readonly IMapper _mapper;

        public PromocodesController(IPromocodeService promocodeService, IMapper mapper)
        {
            _promocodeService = promocodeService ?? throw new ArgumentNullException(nameof(promocodeService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Получить все промокоды.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET /api/promocodes
        ///     {
        ///         "BeginDate": "2023-06-01",
        ///         "EndDate": "2023-09-01",
        ///     }
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PromocodeResponse>> GetPromocodesAsync([FromQuery] DateFilter dateFilter)
        {
            var promocodes = await _promocodeService.GetAllAsync(dateFilter.BeginDate, dateFilter.EndDate);

            return promocodes.Select(_mapper.Map<PromocodeResponse>).ToList();
        }

        /// <summary>
        /// Добавить промокод покупателям.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /api/promocodes
        ///     {
        ///         "Code": "Code",
        ///         "ServiceInfo": "Service info",
        ///         "BeginDate": "2023-06-01",
        ///         "EndDate": "2023-09-01",
        ///         "PartnerName": "Partner name",
        ///         "PartnerManager": {"id": "451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"},
        ///         "Preference": {"id": "ef7f299f-92d7-459f-896e-078ed53ef99c"}
        ///     }
        /// </remarks>
        /// <param name="value">List of customers who have been issued a promo code</param>
        /// <returns>List of customers who have been issued a promo code</returns>
        /// <response code="201">Promocode created</response>        
        [HttpPut]
        [ProducesResponseType(201)]
        public async Task<ActionResult> GivePromocodesToCustomersWithPreferenceAsync([FromBody] PromocodeModel value)
        {
            var customerWithPromocodes = await _promocodeService.GivePromocodesToCustomersWithPreferenceAsync(_mapper.Map<PromocodeDto>(value));
            return Created(nameof(GivePromocodesToCustomersWithPreferenceAsync), customerWithPromocodes.Select(_mapper.Map<PersonResponse>));
        }
    }
}
