﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel;
using Otus.Teaching.PromoCodeFactory.Service;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Покупатели
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;

        public CustomersController(ICustomerService customerService, IMapper mapper)
        {
            _customerService = customerService ?? throw new ArgumentNullException(nameof(customerService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Получить всех покупателей.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PersonResponse>> GetRolesAsync()
        {
            var customers = await _customerService.GetAllAsync();

            var rolesModelList = customers.Select(_mapper.Map<PersonResponse>).ToList();

            return rolesModelList;
        }

        /// <summary>
        /// Получить данные покупателя по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetEmployeeByIdAsync(Guid id)
        {
            CustomerDto customerDto = await _customerService.GetByIdAsync(id);

            var outCustomer = _mapper.Map<CustomerResponse>(customerDto);

            return outCustomer;
        }

        /// <summary>
        /// Добавить покупателя.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /api/customer
        ///     {
        ///         "FirstName": "First name",
        ///         "LastName": "Last name",
        ///         "Email": "Email",
        ///         "Preferences": [{"id": "ef7f299f-92d7-459f-896e-078ed53ef99c"}]
        ///     }
        /// </remarks>
        /// <param name="value">Customer</param>
        /// <returns>Newly created type of customer</returns>
        /// <response code="201">Returns the newly created item</response>        
        [HttpPut]
        [ProducesResponseType(201)]
        public async Task<ActionResult> Create([FromBody] CustomerModel value)
        {
            var newCustomer = await _customerService.CreateAsync(_mapper.Map<CustomerDto>(value));
            return CreatedAtAction(nameof(Create), _mapper.Map<CustomerResponse>(newCustomer));
        }

        /// <summary>
        /// Обновить покупателя.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/customer
        ///     {
        ///         "id": "a6c8c6b1-4349-45b0-ab31-244740aaf0f0",
        ///         "FirstName": "First name",
        ///         "LastName": "Last name",
        ///         "Email": "Email",
        ///         "Preference": {"id": "ef7f299f-92d7-459f-896e-078ed53ef99c"}
        ///     }
        ///     
        /// </remarks>
        /// <param name="value"></param>
        /// <returns>Updated customer</returns>
        /// <response code="202">Returns updated customer with this ID</response>              
        [HttpPost]
        [ProducesResponseType(202)]
        public async Task<ActionResult> Update([FromBody] CustomerModel value)
        {
            var outCustomer = await _customerService.UpdateAsync(_mapper.Map<CustomerDto>(value));
            return AcceptedAtAction(nameof(Update), _mapper.Map<CustomerResponse>(outCustomer));
        }

        /// <summary>
        /// Удалить покупателя.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     Delete /api/Customer/1
        ///     
        /// </remarks>
        /// <param name="id">Id customer</param>        
        /// <response code="200">The item has been removed</response>        
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _customerService.DeleteByIdAsync(id);

            return Ok($"Customer with id = {id} has been removed");
        }
    }
}
