﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeService _employeeService;
        private readonly IMapper _mapper;

        public EmployeesController(IEmployeeService employeeService, IMapper mapper)
        {
            _employeeService = employeeService ?? throw new ArgumentNullException(nameof(employeeService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PersonResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeService.GetAllAsync();

            var employeesModelList = employees.Select(_mapper.Map<PersonResponse>).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeService.GetByIdAsync(id);

            var employeeModel = _mapper.Map<EmployeeResponse>(employee);

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /api/employee
        ///     {
        ///         "FirstName": "First name",
        ///         "LastName": "Last name",
        ///         "Email": "Email",
        ///         "Role": {"id": "53729686-a368-4eeb-8bfa-cc69b6050d02"}
        ///     }
        /// </remarks>
        /// <param name="value">Employee</param>
        /// <returns>Newly created type of employee</returns>
        /// <response code="201">Returns the newly created item</response>
        /// 
        [HttpPut]
        [ProducesResponseType(201)]
        public async Task<ActionResult> Create([FromBody] EmployeeModel value)
        {
            var newEmployee = await _employeeService.CreateAsync(_mapper.Map<EmployeeDto>(value));
            return CreatedAtAction(nameof(Create), _mapper.Map<EmployeeResponse>(newEmployee));
        }

        /// <summary>
        /// Обновить сотрудника.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/Employee
        ///     {
        ///         "id": "451533d5-d8d5-4a11-9c7b-eb9f14e1a32f",
        ///         "FirstName": "First name",
        ///         "LastName": "Last name",
        ///         "Email": "Email",
        ///         "Role": {"id": "53729686-a368-4eeb-8bfa-cc69b6050d02"}
        ///     }
        ///     
        /// </remarks>
        /// <param name="value"></param>
        /// <returns>Updated employee</returns>
        /// <response code="202">Returns updated employee with this ID</response>              
        [HttpPost]
        [ProducesResponseType(202)]
        public async Task<ActionResult> Update([FromBody] EmployeeModel value)
        {
            EmployeeDto outEmployee = await _employeeService.UpdateAsync(_mapper.Map<EmployeeDto>(value));
            return AcceptedAtAction(nameof(Update), _mapper.Map<EmployeeResponse>(outEmployee));
        }

        /// <summary>
        /// Удалить сотрудника.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     Delete /api/Employees/1
        ///     
        /// </remarks>
        /// <param name="id">Id employee</param>        
        /// <response code="200">The item has been removed</response>        
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _employeeService.DeleteByIdAsync(id);

            return Ok($"Employee with id = {id} has been removed");
        }
    }
}