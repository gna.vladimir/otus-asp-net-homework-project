﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.WebHost.Models.ResponseModel;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly IPreferenceService _preferenceService;
        private readonly IMapper _mapper;

        public PreferencesController(IPreferenceService preferenceService, IMapper mapper)
        {
            _preferenceService = preferenceService ?? throw new ArgumentNullException(nameof(preferenceService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Получить все предпочтения.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PreferenceResponse>> GetEmployeesAsync()
        {
            var preference = await _preferenceService.GetAllAsync();

            var preferenceModelList = preference.Select(_mapper.Map<PreferenceResponse>).ToList();

            return preferenceModelList;
        }

        /// <summary>
        /// Добавить предпочтение.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     PUT /api/preference
        ///     {
        ///         "Name": "First name"
        ///     }
        /// </remarks>
        /// <param name="value">Preference</param>
        /// <returns>Newly created type of preference</returns>
        /// <response code="201">Returns the newly created item</response>
        /// 
        [HttpPut]
        [ProducesResponseType(201)]
        public async Task<ActionResult> Create([FromBody] PreferenceModel value)
        {
            PreferenceDto newEmployee = await _preferenceService.CreateAsync(_mapper.Map<PreferenceDto>(value));
            return CreatedAtAction(nameof(Create), _mapper.Map<PreferenceResponse>(newEmployee));
        }

        /// <summary>
        /// Обновить предпочтение.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/preference
        ///     {
        ///         "id": "c4bda62e-fc74-4256-a956-4760b3858cbd",
        ///         "Name": "new name"
        ///     }
        ///     
        /// </remarks>
        /// <param name="value"></param>
        /// <returns>Updated preference</returns>
        /// <response code="202">Returns updated preference with this ID</response>                
        [HttpPost]
        [ProducesResponseType(202)]
        public async Task<ActionResult> Update([FromBody] PreferenceModel value)
        {
            PreferenceDto outPreference = await _preferenceService.UpdateAsync(_mapper.Map<PreferenceDto>(value));
            return AcceptedAtAction(nameof(Update), _mapper.Map<PreferenceResponse>(outPreference));
        }

        /// <summary>
        /// Удалить предпочтение.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     Delete /api/preference/1
        ///     
        /// </remarks>
        /// <param name="id">Id preference</param>        
        /// <response code="200">The item has been removed</response>        
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        public async Task<IActionResult> Delete(Guid id)
        {
            await _preferenceService.DeleteByIdAsync(id);

            return Ok($"Preference with id = {id} has been removed");
        }
    }
}