﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.Service.Exceptions;
using System.Data;

namespace Otus.Teaching.PromoCodeFactory.Service
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IPreferenceRepository _preferenceRepository;
        private readonly IMapper _mapper;

        public CustomerService(ICustomerRepository customerRepository, IPreferenceRepository preferenceRepository, IMapper mapper)
        {
            _customerRepository = customerRepository ?? throw new ArgumentNullException(nameof(customerRepository));
            _preferenceRepository = preferenceRepository ?? throw new ArgumentNullException(nameof(preferenceRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public async Task<CustomerDto> CreateAsync(CustomerDto customerDto)
        {
            Customer newCustomer = _mapper.Map<Customer>(customerDto);
            List<Preference> newPreferences = new();
            if (customerDto.Preferences is not null)
            {
                foreach (var preferenceId in customerDto.Preferences.Select(p => p.Id).Distinct())
                {
                    var newPreference = await _preferenceRepository.GetByIdAsync(preferenceId)
                        ?? throw new NotFoundIdException(preferenceId, nameof(Preference));
                    newPreferences.Add(newPreference);
                }
            }
            newCustomer.Preferences = newPreferences;
            _customerRepository.Add(newCustomer);
            await _customerRepository.SaveChangesAsync();
            return _mapper.Map<CustomerDto>(await _customerRepository.GetByIdAsync(customerDto.Id));
        }

        public async Task DeleteByIdAsync(Guid id)
        {
            Customer customer = await _customerRepository.GetByIdAsync(id)
                ?? throw new NotFoundIdException(id, nameof(Customer));
            _customerRepository.Delete(customer);
            await _customerRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<CustomerDto>> GetAllAsync()
        {
            var Customers = await _customerRepository.GetAllAsync();
            return Customers.Select(_mapper.Map<CustomerDto>);
        }

        public async Task<CustomerDto> GetByIdAsync(Guid id)
        {
            Customer outCustomer = await _customerRepository.GetByIdAsync(id)
                ?? throw new NotFoundIdException(id, nameof(Customer));
            return _mapper.Map<CustomerDto>(outCustomer);
        }

        public async Task<CustomerDto> UpdateAsync(CustomerDto customerDto)
        {
            Customer updateCustomer = await _customerRepository.GetByIdAsync(customerDto.Id)
                ?? throw new NotFoundIdException(customerDto.Id, nameof(Customer));

            _mapper.Map(customerDto, updateCustomer);
            updateCustomer.Preferences = updateCustomer.Preferences;
            if (customerDto.Preferences is not null)
            {
                var updatePreferences = new List<Preference>();
                foreach (var preferenceId in customerDto.Preferences.Select(p => p.Id).Distinct())
                {
                    var updatePreference = await _preferenceRepository.GetByIdAsync(preferenceId)
                        ?? throw new NotFoundIdException(preferenceId, nameof(Preference));
                    updatePreferences.Add(updatePreference);
                }
                updateCustomer.Preferences = updatePreferences;
            }
            _customerRepository.Update(updateCustomer);
            await _customerRepository.SaveChangesAsync();

            return _mapper.Map<CustomerDto>(await _customerRepository.GetByIdAsync(updateCustomer.Id));
        }
    }
}