﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.Service.Exceptions;
using System.Data;

namespace Otus.Teaching.PromoCodeFactory.Service
{
    public class EmployeeService : IEmployeeService
    {
        private const bool NoTracking = true;
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IRoleRepository _roleRepository;
        private readonly IMapper _mapper;

        public EmployeeService(IEmployeeRepository employeeRepository, IRoleRepository roleRepository, IMapper mapper)
        {
            _employeeRepository = employeeRepository ?? throw new ArgumentNullException(nameof(employeeRepository));
            _roleRepository = roleRepository ?? throw new ArgumentNullException(nameof(roleRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public async Task<EmployeeDto> CreateAsync(EmployeeDto employeeDto)
        {
            Employee newEmployee = _mapper.Map<Employee>(employeeDto);
            Role? newRole = (await _roleRepository.GetAllAsync()).FirstOrDefault(x => x.Name == "Guest");
            if (employeeDto.Role is not null)
            {
                newRole = await _roleRepository.GetByIdAsync(employeeDto.Role.Id)
                    ?? throw new NotFoundIdException(employeeDto.Role.Id, nameof(Role));
            }
            newEmployee.Role = newRole;
            _employeeRepository.Add(newEmployee);
            await _employeeRepository.SaveChangesAsync();
            return _mapper.Map<EmployeeDto>(await _employeeRepository.GetByIdAsync(employeeDto.Id));
        }

        public async Task DeleteByIdAsync(Guid id)
        {
            Employee employee = await _employeeRepository.GetByIdAsync(id)
                ?? throw new NotFoundIdException(id, nameof(Employee));
            _employeeRepository.Delete(employee);
            await _employeeRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<EmployeeDto>> GetAllAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();
            return employees.Select(_mapper.Map<EmployeeDto>);
        }

        public async Task<EmployeeDto> GetByIdAsync(Guid id)
        {
            Employee outEmployee = await _employeeRepository.GetByIdAsync(id)
                ?? throw new NotFoundIdException(id, nameof(Employee));
            return _mapper.Map<EmployeeDto>(outEmployee);
        }

        public async Task<EmployeeDto> UpdateAsync(EmployeeDto employeeDto)
        {
            Employee exist = await _employeeRepository.GetByIdAsync(employeeDto.Id, NoTracking)
                ?? throw new NotFoundIdException(employeeDto.Id, nameof(Employee));

            _mapper.Map(employeeDto, exist);
            if (employeeDto.Role is not null)
            {
                exist.Role = await _roleRepository.GetByIdAsync(employeeDto.Role.Id)
                    ?? throw new NotFoundIdException(employeeDto.Role.Id, nameof(Role));
            }

            _employeeRepository.Update(exist);
            await _employeeRepository.SaveChangesAsync();

            return _mapper.Map<EmployeeDto>(await _employeeRepository.GetByIdAsync(employeeDto.Id));
        }
    }
}