﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Service.Mapping
{
    public class PromocodeProfile : Profile
    {
        public PromocodeProfile()
        {
            CreateMap<PromocodeDto, Promocode>()
                .ForMember(x => x.PartnerManager, map => map.Ignore())
                .ForMember(x => x.Preference, map => map.Ignore())
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember is not null));

            CreateMap<Promocode, PromocodeDto>();
        }
    }
}
