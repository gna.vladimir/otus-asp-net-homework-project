﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.Service.Abstractions;
using Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.Service.Exceptions;

namespace Otus.Teaching.PromoCodeFactory.Service
{
    public class PromocodeService : IPromocodeService
    {
        private const bool NOTRACKING = true;
        private readonly IPromocodeRepository _promocodeRepository;
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;

        public PromocodeService(IPromocodeRepository promocodeRepository, ICustomerRepository customerRepository, IMapper mapper)
        {
            _promocodeRepository = promocodeRepository ?? throw new ArgumentNullException(nameof(promocodeRepository));
            _customerRepository = customerRepository ?? throw new ArgumentNullException(nameof(customerRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<PromocodeDto>> GetAllAsync(string inBeginDate, string inEndDate)
        {
            if (string.IsNullOrEmpty(inBeginDate) || string.IsNullOrEmpty(inEndDate) ||
                !DateOnly.TryParse(inBeginDate, out DateOnly beginDate) || !DateOnly.TryParse(inEndDate, out DateOnly endDate))
            {
                throw new InvalidDataException("Date is not correct");
            }

            if (beginDate > endDate)
            {
                throw new InvalidDataException("Begin date can't be greater than end date");
            }
            var promocodes = (await _promocodeRepository.GetAllAsync()).Where(x => (x.BeginDate <= beginDate && x.EndDate >= beginDate)
                || (x.EndDate >= endDate && x.BeginDate <= endDate)
                || (x.BeginDate >= beginDate && x.EndDate <= endDate));
            return promocodes.Select(_mapper.Map<PromocodeDto>);
        }

        public async Task<IEnumerable<CustomerDto>> GivePromocodesToCustomersWithPreferenceAsync(PromocodeDto promocodeDto)
        {
            var preferenseId = promocodeDto.Preference?.Id
                ?? throw new ArgumentNullException(nameof(promocodeDto.Preference), "Preference can't be null");

            var customers = (await _customerRepository.GetAllIncludeAsync()).Where(c => c.Preferences.Select(p => p.Id).Contains(preferenseId)).ToList();
            if (!customers.Any())
            {
                throw new InvalidDataException($"Not customers found with id preference: {preferenseId}");
            }

            var newPromocode = _mapper.Map<Promocode>(promocodeDto);
            newPromocode.Customers = customers;                       

            _promocodeRepository.Add(newPromocode);
            await _promocodeRepository.SaveChangesAsync();

            return customers.Select(_mapper.Map<CustomerDto>);
        }
    }
}