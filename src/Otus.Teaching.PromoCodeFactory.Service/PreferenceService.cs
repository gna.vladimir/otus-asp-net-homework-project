﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.Service.Exceptions;
using System.Data;
using System.Runtime.Serialization;

namespace Otus.Teaching.PromoCodeFactory.Service
{
    public class PreferenceService : IPreferenceService
    {
        private readonly IPreferenceRepository _preferenceRepository;
        private readonly IMapper _mapper;

        public PreferenceService(IPreferenceRepository preferenceRepository, IMapper mapper)
        {
            _preferenceRepository = preferenceRepository ?? throw new ArgumentNullException(nameof(preferenceRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public async Task<PreferenceDto> CreateAsync(PreferenceDto preferenceDto)
        {
            Preference newPreference = _mapper.Map<Preference>(preferenceDto);
            _preferenceRepository.Add(newPreference);
            await _preferenceRepository.SaveChangesAsync();
            return _mapper.Map<PreferenceDto>(await _preferenceRepository.GetByIdAsync(preferenceDto.Id));
        }

        public async Task DeleteByIdAsync(Guid id)
        {
            Preference preference = await _preferenceRepository.GetByIdAsync(id)
                ?? throw new NotFoundIdException(id, nameof(Preference));
            _preferenceRepository.Delete(preference);
            await _preferenceRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<PreferenceDto>> GetAllAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            return preferences.Select(_mapper.Map<PreferenceDto>);
        }

        public async Task<PreferenceDto> UpdateAsync(PreferenceDto preferenceDto)
        {
            Preference exist = await _preferenceRepository.GetByIdAsync(preferenceDto.Id, true)
                ?? throw new NotFoundIdException(preferenceDto.Id, nameof(Preference));

            _mapper.Map(preferenceDto, exist);

            _preferenceRepository.Update(exist);
            await _preferenceRepository.SaveChangesAsync();

            return _mapper.Map<PreferenceDto>(await _preferenceRepository.GetByIdAsync(preferenceDto.Id));
        }


    }
}