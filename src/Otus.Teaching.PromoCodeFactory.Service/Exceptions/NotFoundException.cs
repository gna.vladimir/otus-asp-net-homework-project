﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Service.Exceptions
{
    [Serializable]
    internal class NotFoundIdException : Exception
    {
        public NotFoundIdException() : base()
        {
        }

        public NotFoundIdException(string? message) : base(message)
        {
        }

        /// <summary>
        /// Throw exception not found id.
        /// </summary>
        /// <param name="id">Id entity.</param>
        /// <param name="type">Name entity.</param>
        /// <exception cref="KeyNotFoundException"></exception>
        public NotFoundIdException(Guid id, string type)
        {
            throw new KeyNotFoundException($"Not found {type} with this id: {id}");
        }
    }
}
