﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Service.DTO.Administration
{
    public class RoleDto : BaseEntityDto
    {
        public string? Name { get; set; }

        public string? Description { get; set; }
    }
}
