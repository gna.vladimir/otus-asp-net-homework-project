﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Service.DTO.Administration
{
    public class EmployeeDto : BasePersonDto
    {
        public RoleDto? Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}
