﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment
{
    public class PreferenceDto : BaseEntityDto
    {
        public string? Name { get; set; }

        public List<CustomerDto>? Customers { get; set; }
    }
}
