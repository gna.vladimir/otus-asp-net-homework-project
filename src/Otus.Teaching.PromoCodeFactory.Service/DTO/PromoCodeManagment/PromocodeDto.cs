﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment
{
    public class PromocodeDto : BaseEntityDto
    {
        public string? Code { get; set; }

        public string? ServiceInfo { get; set; }

        public DateOnly? BeginDate { get; set; }

        public DateOnly? EndDate { get; set; }

        public string? PartnerName { get; set; }

        public EmployeeDto? PartnerManager { get; set; }

        public PreferenceDto? Preference { get; set; }

        public List<CustomerDto>? Customers { get; set; }
    }
}
