﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Service.DTO
{
    public class BaseEntityDto
    {
        public Guid Id { get; set; }
    }
}
