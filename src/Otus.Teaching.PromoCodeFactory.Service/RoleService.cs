﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Service.Abstractions;
using Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;

namespace Otus.Teaching.PromoCodeFactory.Service
{
    public class RoleService : IRoleService
    {
        private readonly IRoleRepository _roleRepository;
        private readonly IMapper _mapper;

        public RoleService(IRoleRepository roleRepository, IMapper mapper)
        {
            _roleRepository = roleRepository ?? throw new ArgumentNullException(nameof(roleRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        
        public async Task<IEnumerable<RoleDto>> GetAllAsync()
        {
            var roles = await _roleRepository.GetAllAsync();
            return roles.Select(_mapper.Map<RoleDto>);
        }        
    }
}