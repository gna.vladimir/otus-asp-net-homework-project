﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces
{
    public interface IEmployeeService
    {
        Task<IEnumerable<EmployeeDto>> GetAllAsync();

        Task<EmployeeDto> GetByIdAsync(Guid id);

        Task<EmployeeDto> CreateAsync(EmployeeDto employeeDto);

        Task<EmployeeDto> UpdateAsync(EmployeeDto employeeDto);

        Task DeleteByIdAsync(Guid id);
    }
}