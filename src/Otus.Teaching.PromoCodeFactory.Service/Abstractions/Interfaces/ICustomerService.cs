﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces
{
    public interface ICustomerService
    {
        Task<IEnumerable<CustomerDto>> GetAllAsync();

        Task<CustomerDto> GetByIdAsync(Guid id);

        Task<CustomerDto> CreateAsync(CustomerDto CustomerDto);

        Task<CustomerDto> UpdateAsync(CustomerDto CustomerDto);

        Task DeleteByIdAsync(Guid id);
    }
}