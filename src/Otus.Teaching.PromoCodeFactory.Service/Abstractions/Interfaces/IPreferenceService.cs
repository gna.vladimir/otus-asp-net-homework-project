﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces
{
    public interface IPreferenceService
    {
        Task<IEnumerable<PreferenceDto>> GetAllAsync();

        Task<PreferenceDto> CreateAsync(PreferenceDto preferenceDto);

        Task<PreferenceDto> UpdateAsync(PreferenceDto preferenceDto);

        Task DeleteByIdAsync(Guid id);
    }
}