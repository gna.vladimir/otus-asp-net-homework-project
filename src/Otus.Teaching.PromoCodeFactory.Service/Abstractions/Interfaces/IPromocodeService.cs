﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Service.DTO.PromoCodeManagment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Service.Abstractions.Interfaces
{
    public interface IPromocodeService
    {
        Task<IEnumerable<PromocodeDto>> GetAllAsync(string beginDate, string endDate);

        Task<IEnumerable<CustomerDto>> GivePromocodesToCustomersWithPreferenceAsync(PromocodeDto promocodeDto);
    }
}