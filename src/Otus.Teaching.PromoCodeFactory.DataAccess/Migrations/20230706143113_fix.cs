﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class fix : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CustomersPreferences",
                keyColumns: new[] { "CustomersId", "PreferencesId" },
                keyValues: new object[] { new Guid("3d5dc2b3-3880-49e7-9e1f-93cc98c067ea"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") });

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("3d5dc2b3-3880-49e7-9e1f-93cc98c067ea"));

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("5ec1be54-f415-4e22-b708-c4fed95bd7c2"), "petr_vasiliev@mail.ru", "Петр", "Васильев" });

            migrationBuilder.InsertData(
                table: "CustomersPreferences",
                columns: new[] { "CustomersId", "PreferencesId" },
                values: new object[] { new Guid("5ec1be54-f415-4e22-b708-c4fed95bd7c2"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "CustomersPreferences",
                keyColumns: new[] { "CustomersId", "PreferencesId" },
                keyValues: new object[] { new Guid("5ec1be54-f415-4e22-b708-c4fed95bd7c2"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") });

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("5ec1be54-f415-4e22-b708-c4fed95bd7c2"));

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("3d5dc2b3-3880-49e7-9e1f-93cc98c067ea"), "petr_vasiliev@mail.ru", "Петр", "Васильев" });

            migrationBuilder.InsertData(
                table: "CustomersPreferences",
                columns: new[] { "CustomersId", "PreferencesId" },
                values: new object[] { new Guid("3d5dc2b3-3880-49e7-9e1f-93cc98c067ea"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") });
        }
    }
}
