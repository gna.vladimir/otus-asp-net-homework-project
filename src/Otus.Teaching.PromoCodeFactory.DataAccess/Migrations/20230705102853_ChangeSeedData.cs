﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class ChangeSeedData : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Promocodes_Customers_CustomerId",
                table: "Promocodes");

            migrationBuilder.DeleteData(
                table: "Promocodes",
                keyColumn: "Id",
                keyValue: new Guid("fb851677-28a5-48af-9c27-06fbc617fe0b"));

            migrationBuilder.AlterColumn<Guid>(
                name: "CustomerId",
                table: "Promocodes",
                type: "TEXT",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                column: "LastName",
                value: "Сергеев");

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("8148a2c0-a034-4808-aa73-fddef5ff6610"), "petr_vasiliev@mail.ru", "Петр", "Васильев" });

            migrationBuilder.InsertData(
                table: "CustomersPreferences",
                columns: new[] { "CustomersId", "PreferencesId" },
                values: new object[,]
                {
                    { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") },
                    { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c") }
                });

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                column: "AppliedPromocodesCount",
                value: 0);

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: new Guid("f766e2bf-340a-46ea-bff3-f1700b435895"),
                column: "AppliedPromocodesCount",
                value: 0);

            migrationBuilder.InsertData(
                table: "CustomersPreferences",
                columns: new[] { "CustomersId", "PreferencesId" },
                values: new object[] { new Guid("8148a2c0-a034-4808-aa73-fddef5ff6610"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") });

            migrationBuilder.AddForeignKey(
                name: "FK_Promocodes_Customers_CustomerId",
                table: "Promocodes",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Promocodes_Customers_CustomerId",
                table: "Promocodes");

            migrationBuilder.DeleteData(
                table: "CustomersPreferences",
                keyColumns: new[] { "CustomersId", "PreferencesId" },
                keyValues: new object[] { new Guid("8148a2c0-a034-4808-aa73-fddef5ff6610"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") });

            migrationBuilder.DeleteData(
                table: "CustomersPreferences",
                keyColumns: new[] { "CustomersId", "PreferencesId" },
                keyValues: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") });

            migrationBuilder.DeleteData(
                table: "CustomersPreferences",
                keyColumns: new[] { "CustomersId", "PreferencesId" },
                keyValues: new object[] { new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"), new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c") });

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("8148a2c0-a034-4808-aa73-fddef5ff6610"));

            migrationBuilder.AlterColumn<Guid>(
                name: "CustomerId",
                table: "Promocodes",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "TEXT");

            migrationBuilder.UpdateData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                column: "LastName",
                value: "Петров");

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                column: "AppliedPromocodesCount",
                value: 5);

            migrationBuilder.UpdateData(
                table: "Employees",
                keyColumn: "Id",
                keyValue: new Guid("f766e2bf-340a-46ea-bff3-f1700b435895"),
                column: "AppliedPromocodesCount",
                value: 10);

            migrationBuilder.InsertData(
                table: "Promocodes",
                columns: new[] { "Id", "BeginDate", "Code", "CustomerId", "EndDate", "PartnerManagerId", "PartnerName", "PreferenceId", "ServiceInfo" },
                values: new object[] { new Guid("fb851677-28a5-48af-9c27-06fbc617fe0b"), new DateTime(2023, 6, 28, 11, 13, 10, 545, DateTimeKind.Local).AddTicks(4471), "some code", null, new DateTime(2023, 9, 28, 11, 13, 10, 545, DateTimeKind.Local).AddTicks(4479), new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"), "some name partner", new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), "some information" });

            migrationBuilder.AddForeignKey(
                name: "FK_Promocodes_Customers_CustomerId",
                table: "Promocodes",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id");
        }
    }
}
