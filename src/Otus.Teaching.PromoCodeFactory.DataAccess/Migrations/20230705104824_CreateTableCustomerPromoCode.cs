﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class CreateTableCustomerPromoCode : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Promocodes_Customers_CustomerId",
                table: "Promocodes");

            migrationBuilder.DropIndex(
                name: "IX_Promocodes_CustomerId",
                table: "Promocodes");

            migrationBuilder.DeleteData(
                table: "CustomersPreferences",
                keyColumns: new[] { "CustomersId", "PreferencesId" },
                keyValues: new object[] { new Guid("8148a2c0-a034-4808-aa73-fddef5ff6610"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") });

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("8148a2c0-a034-4808-aa73-fddef5ff6610"));

            migrationBuilder.DropColumn(
                name: "CustomerId",
                table: "Promocodes");

            migrationBuilder.CreateTable(
                name: "CustomerPromocode",
                columns: table => new
                {
                    CustomersId = table.Column<Guid>(type: "TEXT", nullable: false),
                    PromocodesId = table.Column<Guid>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CustomerPromocode", x => new { x.CustomersId, x.PromocodesId });
                    table.ForeignKey(
                        name: "FK_CustomerPromocode_Customers_CustomersId",
                        column: x => x.CustomersId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CustomerPromocode_Promocodes_PromocodesId",
                        column: x => x.PromocodesId,
                        principalTable: "Promocodes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("3d5dc2b3-3880-49e7-9e1f-93cc98c067ea"), "petr_vasiliev@mail.ru", "Петр", "Васильев" });

            migrationBuilder.InsertData(
                table: "CustomersPreferences",
                columns: new[] { "CustomersId", "PreferencesId" },
                values: new object[] { new Guid("3d5dc2b3-3880-49e7-9e1f-93cc98c067ea"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") });

            migrationBuilder.CreateIndex(
                name: "IX_CustomerPromocode_PromocodesId",
                table: "CustomerPromocode",
                column: "PromocodesId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CustomerPromocode");

            migrationBuilder.DeleteData(
                table: "CustomersPreferences",
                keyColumns: new[] { "CustomersId", "PreferencesId" },
                keyValues: new object[] { new Guid("3d5dc2b3-3880-49e7-9e1f-93cc98c067ea"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") });

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "Id",
                keyValue: new Guid("3d5dc2b3-3880-49e7-9e1f-93cc98c067ea"));

            migrationBuilder.AddColumn<Guid>(
                name: "CustomerId",
                table: "Promocodes",
                type: "TEXT",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "Id", "Email", "FirstName", "LastName" },
                values: new object[] { new Guid("8148a2c0-a034-4808-aa73-fddef5ff6610"), "petr_vasiliev@mail.ru", "Петр", "Васильев" });

            migrationBuilder.InsertData(
                table: "CustomersPreferences",
                columns: new[] { "CustomersId", "PreferencesId" },
                values: new object[] { new Guid("8148a2c0-a034-4808-aa73-fddef5ff6610"), new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84") });

            migrationBuilder.CreateIndex(
                name: "IX_Promocodes_CustomerId",
                table: "Promocodes",
                column: "CustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Promocodes_Customers_CustomerId",
                table: "Promocodes",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
