﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class DeleteFullNameEmployee : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Promocodes",
                keyColumn: "Id",
                keyValue: new Guid("1f9299b7-a3dd-480d-8c0b-01d0dfab8a4e"));

            migrationBuilder.InsertData(
                table: "Promocodes",
                columns: new[] { "Id", "BeginDate", "Code", "CustomerId", "EndDate", "PartnerManagerId", "PartnerName", "PreferenceId", "ServiceInfo" },
                values: new object[] { new Guid("fb851677-28a5-48af-9c27-06fbc617fe0b"), new DateTime(2023, 6, 28, 11, 13, 10, 545, DateTimeKind.Local).AddTicks(4471), "some code", null, new DateTime(2023, 9, 28, 11, 13, 10, 545, DateTimeKind.Local).AddTicks(4479), new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"), "some name partner", new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), "some information" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Promocodes",
                keyColumn: "Id",
                keyValue: new Guid("fb851677-28a5-48af-9c27-06fbc617fe0b"));

            migrationBuilder.InsertData(
                table: "Promocodes",
                columns: new[] { "Id", "BeginDate", "Code", "CustomerId", "EndDate", "PartnerManagerId", "PartnerName", "PreferenceId", "ServiceInfo" },
                values: new object[] { new Guid("1f9299b7-a3dd-480d-8c0b-01d0dfab8a4e"), new DateTime(2023, 6, 28, 1, 3, 29, 582, DateTimeKind.Local).AddTicks(9754), "some code", null, new DateTime(2023, 9, 28, 1, 3, 29, 582, DateTimeKind.Local).AddTicks(9759), new Guid("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"), "some name partner", new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"), "some information" });
        }
    }
}
