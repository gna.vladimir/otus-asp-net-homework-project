﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Abstractions
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id, bool noTracking = false)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public void Add(T item)
        {
            var tempData = Data.ToList();
            tempData.Add(item);
            Data = tempData;
        }

        public void Update(T item)
        {
            Delete(GetByIdAsync(item.Id).Result);
            Data = Data.Append(item);
        }

        public void Delete(T item)
        {
            var tempData = Data.ToList();
            tempData.Remove(item);
            Data = tempData;
        }
    }
}