﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Abstractions
{
    public class EFRepository<T> : IRepository<T>
        where T : BaseEntity
    {

        protected readonly DbContext _context;
        private readonly DbSet<T> _entitySet;

        protected EFRepository(DbContext context)
        {
            _context = context;
            _entitySet = _context.Set<T>();
        }

        public virtual void Add(T item)
        {
            _entitySet.Add(item);
        }

        public virtual void Delete(T item)
        {
            _entitySet.Remove(item);
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _entitySet.ToListAsync();
        }

        public virtual async Task<T> GetByIdAsync(Guid id, bool noTracking = false)
        {
            return noTracking ? await _entitySet.AsNoTracking().FirstOrDefaultAsync(x=>x.Id == id):
                await _entitySet.FindAsync(id);
        }

        public virtual void Update(T item)
        {            
            _context.Entry(item).State = EntityState.Modified;
        }

        public virtual async Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
