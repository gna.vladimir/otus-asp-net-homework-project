﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public sealed class EmployeeRepository : EFRepository<Employee>, IEmployeeRepository
    {
        private readonly DbSet<Employee> _entitySet;
        public EmployeeRepository(PromocodeContext context) : base(context)
        {
            _entitySet = context.Employees;
        }
        
        public override async Task<Employee> GetByIdAsync(Guid id, bool noTracking)
        {
            return noTracking ? await _entitySet.AsNoTracking().Include(x => x.Role).FirstOrDefaultAsync(x => x.Id == id): 
            await _entitySet.Include(x => x.Role).FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}
