﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public sealed class CustomerRepository : EFRepository<Customer>, ICustomerRepository
    {
        private readonly DbSet<Customer> _entitySet;
        public CustomerRepository(PromocodeContext context) : base(context)
        {
            _entitySet = context.Customers;
        }
        public override async Task<Customer> GetByIdAsync(Guid id, bool noTracking)
        {
            return noTracking ? await _entitySet.Include(p => p.Preferences).Include(x => x.Promocodes).ThenInclude(x=>x.PartnerManager).AsNoTracking().FirstOrDefaultAsync(x => x.Id == id) :
            await _entitySet.Include(p => p.Preferences).Include(x => x.Promocodes).ThenInclude(x => x.PartnerManager).FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Customer>> GetAllIncludeAsync(bool noTracking = false)
        {
            return noTracking ? await _entitySet.Include(x => x.Preferences).Include(x => x.Promocodes).ThenInclude(x => x.PartnerManager).AsNoTracking().ToListAsync() :
                await _entitySet.Include(x => x.Preferences).Include(x => x.Promocodes).ThenInclude(x => x.PartnerManager).ToListAsync();
        }
    }
}
