﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public sealed class PromocodeRepository : EFRepository<Promocode>, IPromocodeRepository
    {
        private readonly DbSet<Promocode> _entitySet;
        public PromocodeRepository(PromocodeContext context) : base(context)
        {
            _entitySet = context.Promocodes;
        }
        public override async Task<IEnumerable<Promocode>> GetAllAsync()
        {
            return await _entitySet.Include(x => x.Preference).Include(p => p.Customers).Include(e=>e.PartnerManager).ToListAsync();
        }

    }
}
