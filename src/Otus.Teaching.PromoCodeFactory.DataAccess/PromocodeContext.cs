﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class PromocodeContext : DbContext
    {
        public PromocodeContext(DbContextOptions<PromocodeContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<Promocode> Promocodes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region TestData
            IEnumerable<Role> roles = new List<Role>()
            {
                new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            },
            new Role()
            {
                Id = Guid.Parse("73745220-8b23-445c-83b1-ae3662dce2b2"),
                Name = "Guest",
                Description = "Гость"
            }};

            IEnumerable<Employee> employees = new List<Employee>()
            {
                new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                RoleId = roles.FirstOrDefault(x => x.Name == "Admin").Id,
                AppliedPromocodesCount = 0
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                RoleId = roles.FirstOrDefault(x => x.Name == "PartnerManager").Id,
                AppliedPromocodesCount = 0
            }
            };

            IEnumerable<Preference> preferencees = new List<Preference>() {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр"
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья"
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
            };

            IEnumerable<Customer> customeres = new List<Customer>()
            {
                new Customer()
                    {
                        Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Сергеев"
                    },
                new Customer()
                    {
                        Id = Guid.NewGuid(),
                        Email = "petr_vasiliev@mail.ru",
                        FirstName = "Петр",
                        LastName = "Васильев"
                    }
            };
            #endregion

            modelBuilder.Entity<Employee>().HasData(employees);

            modelBuilder.Entity<Role>().HasData(roles);

            modelBuilder.Entity<Preference>().HasData(preferencees);

            modelBuilder.Entity<Customer>().HasData(customeres);

            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Preferences)
                .WithMany(s => s.Customers)
                .UsingEntity(j => j.ToTable("CustomersPreferences")
                .HasData(new[]
                {
                    new { CustomersId = customeres.First().Id, PreferencesId = preferencees.First().Id },
                    new { CustomersId = customeres.First().Id, PreferencesId = preferencees.Last().Id },
                    new { CustomersId = customeres.Last().Id, PreferencesId = preferencees.Last().Id }
                }
                ));
        }
    }
}
