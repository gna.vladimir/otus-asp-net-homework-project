﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BasePerson
    {
        [Required]
        public Guid RoleId { get; set; }

        public Role Role { get; set; }
        
        public List<Promocode> Promocode { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}