﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment
{
    public class Customer : BasePerson
    {
        public List<Preference> Preferences { get; set; } = new List<Preference>();

        public List<Promocode> Promocodes { get; set; }
    }
}
