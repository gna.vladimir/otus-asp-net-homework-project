﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagment
{
    public class Promocode : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public string Code { get; set; }

        [MaxLength(500)]
        public string ServiceInfo { get; set; }

        [Required]
        public DateOnly BeginDate { get; set; }

        [Required]
        public DateOnly EndDate { get; set; }

        [Required]
        [MaxLength(100)]
        public string PartnerName { get; set; }

        [Required]
        public Guid PartnerManagerId { get; set; }

        public Employee PartnerManager { get; set; }

        [Required]
        public Guid PreferenceId { get; set; }

        public Preference Preference { get; set; }

        public List<Customer> Customers { get; set; }

    }
}
